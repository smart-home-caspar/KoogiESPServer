#ifndef LEDPAGE_H
#define LEDPAGE_H

#include "page.h"
#include "iprocess.h"
#include "ledcontroller.h"

class LedPage : public Page
{
protected:
  LedController* m_ledController;

public:
  LedPage(LedController* ledController) : m_ledController(ledController) { }

protected:
  virtual IProcess* createProcess(const Arguments& arguments) const = 0;

  NeoPixel* strip() const
  {
    return m_ledController->strip();
  }

  void process(const Arguments& arguments) override
  {
    m_ledController->setProcess(createProcess(arguments));
  }
};

#endif
