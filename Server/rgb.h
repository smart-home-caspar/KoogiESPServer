#ifndef RGB_H
#define RGB_H

struct RGB{
  int red = 0;
  int green = 0;
  int blue = 0;

  RGB()
    : red(0)
    , green(0)
    , blue(0)
  {
  }

  RGB(int r, int g, int b)
    : red(r)
    , green(g)
    , blue(b)
  {
  }

  bool operator== (const RGB& other) {
    return (red == other.red && green == other.green && blue == other.blue);
  }
};

#endif
