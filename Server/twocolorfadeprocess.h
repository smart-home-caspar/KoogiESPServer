#ifndef TWOCOLORFADEPROCESS_H
#define TWOCOLORFADEPROCESS_H

#include "iprocess.h"
#include "utilitys.h"
#include "neopixel.h"

static const int minNumOfSteps = 30;
static int redStep;
static int greenStep;
static int blueStep;

class TwoColorFadeProcess : public IProcess
{
private:
  NeoPixel* m_strip;
  RGB m_color1;
  RGB m_color2;
  uint32_t m_wait;
  int m_iterator;
  bool m_isReverse;
  int m_redStart;
  int m_greenStart;
  int m_blueStart;
  int m_numOfSteps;
public:
  TwoColorFadeProcess(NeoPixel* strip, RGB color1, RGB color2, uint32_t wait)
    : m_strip(strip)
    , m_color1(color1)
    , m_color2(color2)
    , m_wait(wait)
    , m_iterator(0)
    , m_isReverse(false)
    , m_numOfSteps(minNumOfSteps + wait)
  {
    m_redStart = m_color1.red;
    m_greenStart = m_color1.green;
    m_blueStart = m_color1.blue;
    redStep = stepSizeTimesHundred(m_color1.red, m_color2.red, m_numOfSteps);
    greenStep = stepSizeTimesHundred(m_color1.green, m_color2.green, m_numOfSteps);
    blueStep = stepSizeTimesHundred(m_color1.blue, m_color2.blue, m_numOfSteps);
  }
  
  void run() override
  {
    if (m_iterator >= m_numOfSteps)
      m_isReverse = true;
    else if (m_iterator <= 0)
      m_isReverse = false;
    int red;
    int green;
    int blue;
    if (m_color1.red < m_color2.red)
      red = (m_iterator * redStep/100 + m_redStart);
    else
      red = (m_redStart - m_iterator * redStep/100);
    if (m_color1.green < m_color2.green)
      green = (m_iterator * greenStep/100 + m_greenStart);
    else
      green = (m_greenStart - m_iterator * greenStep/100);
    if (m_color1.blue < m_color2.blue)
      blue = (m_iterator * blueStep/100 + m_blueStart);
    else
      blue = (m_blueStart - m_iterator * blueStep/100);
    
    for (int i = 0; i < m_strip->numPixels(); i++){
      m_strip->setPixelColor(i, RGB(red, green, blue));
    }
    m_strip->show();
    sleepFor(m_wait);
    
    if (m_isReverse)
      m_iterator--;
    else
      m_iterator++;
  }
};

#endif
