#ifndef IPROCESS_H
#define IPROCESS_H

class IProcess
{
private:
  int m_sleepStart = 0;
  int m_currentSleepTime = 0;

public:
  virtual void run() = 0;

  virtual bool isSleeping() {
    return millis() < m_sleepStart + m_currentSleepTime;
  }

protected:
  void sleepFor(int milliSeconds) {
    m_currentSleepTime = milliSeconds;
    m_sleepStart = millis();
  }
};

#endif
