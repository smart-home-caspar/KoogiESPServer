#ifndef LEDWEBSERVER_H
#define LEDWEBSERVER_H

#include "page.h"
#include "wifi.h"
#include "wifiwebserver.h"

#include <vector>

const char* ssid = "Vanemuine";
const char* password = "D6D70263BD";

class LedWebServer : public IProcess
{
private:
  Wifi* m_wifi;
  WifiWebServer* m_server;
  std::vector<Page*> m_pages;
public:
  LedWebServer(Wifi* wifi, WifiWebServer* server)
    : m_wifi(wifi)
    , m_server(server)
  {
    m_server->onNotFound([=] () { handleNotFound(); });
  }

  void addPage(Page* page)
  {
    m_pages.push_back(page);
  }

  const std::vector<Page*>& pages() const
  {
    return m_pages;
  }

  void run() override
  {
    m_server->service();
  }

  void start()
  {
    m_wifi->connect(ssid, password);

    Serial.print("\nConnected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(m_wifi->ip());

    setupPages();

    m_server->begin();

    Serial.println("HTTP server started");
  }

private:
  void setupPages()
  {
    for (auto page : m_pages)
      m_server->on(page->url().c_str(), [=] () { page->handleRequest(m_server); });
  }

  void handleNotFound()
  {
    m_server->send(404, "text/plain", "Page Not Found");
  }

};

#endif
