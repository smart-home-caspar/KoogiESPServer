#ifndef SILINDERROTATEPROCESS_H
#define SILINDERROTATEPROCESS_H

#include "iprocess.h"
#include "utilitys.h"
#include "neopixel.h"

class SilinderRotateProcess : public IProcess
{
private:
  NeoPixel* m_strip;
  RGB m_color;
  int m_wait;
  int m_column;
  double m_columns;
public:
  SilinderRotateProcess(NeoPixel* strip, double columns, RGB color, int wait)
    : m_strip(strip)
    , m_color(color)
    , m_wait(wait)
    , m_column(0)
    , m_columns(columns)
  {
  }
  
  void run() override
  {
    m_strip->setAll(RGB());
    RGB superLightColor = divideColor(20);
    RGB lightColor = divideColor(7);

    setColumnColor(getPreviousColumn(getPreviousColumn(m_column)), superLightColor);
    setColumnColor(getPreviousColumn(m_column), lightColor);
    setColumnColor(m_column, m_color);
    setColumnColor(getNextColumn(m_column), lightColor);
    setColumnColor(getNextColumn(getNextColumn(m_column)), superLightColor);
    advanceToNextColumn();
    m_strip->show();
    sleepFor(m_wait);
  }

  void setColumnColor(int column, RGB color) {
    for (double i = (double)column; (int)i < m_strip->numPixels(); i+=m_columns) {
      m_strip->setPixelColor((int)i, color);
    }
  }

  void advanceToNextColumn()
  {
    m_column = getNextColumn(m_column);
  }

  int getNextColumn(int column) {
    if (column >= m_columns) {
      return 0;
    } else {
      return column + 1;
    }
  }

  int getPreviousColumn(int column) {
    if (column <= 0) {
      return m_columns - 1;
    } else {
      return column - 1;
    }
  }

  RGB divideColor(int factor) {
    return RGB((int)(m_color.red/factor), (int)(m_color.green/factor), (int)(m_color.blue/factor));
  }

};

#endif
