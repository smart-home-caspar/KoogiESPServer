#ifndef ARGUMENTS_H
#define ARGUMENTS_H

#include <vector>
#include <map>

class Arguments {
private:
  struct Argument {
    String key;
    String value;

    Argument(const String& k, const String& v)
      : key(k)
      , value(v)
      {
      }
  };

  std::vector<Argument> m_arguments;

public:
  static Arguments fromMap(const std::map<String, String>& map) {
    Arguments result;
    for (auto it = map.begin(); it != map.end(); it++ )
      result.m_arguments.push_back(Argument(it->first, it->second));
    return result;
  }

  const String& arg(String name) const {
    for (Argument a : m_arguments)
      if (a.key == name)
        return a.value;
    return "";
  }

  const String& arg(int i) const {
    if (i < m_arguments.size())
      return m_arguments[i].value;
    return "";
  }

  const String& argName(int i) const {
    if (i < m_arguments.size())
      return m_arguments[i].key;
    return "";
  }

  int count() const {
    return m_arguments.size();
  }

  bool hasArg(const String& name) const {
    for (Argument a : m_arguments)
      if (a.key == name)
        return true;
    return false;
  }
};

#endif
