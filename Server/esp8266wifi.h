#ifndef ESP8266WIFI_H
#define ESP8266WIFI_H

#include "wifi.h"

#include <ESP8266WiFi.h>

class ESP8266Wifi : public Wifi
{
public:
  void connect(const char* ssid, const char* password) override
  {
    WiFi.mode(WIFI_AP_STA);
    while (!isWifiConnected()) {
      WiFi.begin(ssid, password);
      Serial.println("WiFi failed, retrying.");
      delay(200);
    }
  }

  const String ip()
  {
    return WiFi.localIP().toString();
  }

private:
  bool isWifiConnected()
  {
    return WiFi.waitForConnectResult() == WL_CONNECTED;
  }
};

#endif
