#ifndef SOLIDCOLORPAGE_H
#define SOLIDCOLORPAGE_H

#include "ledpage.h"
#include "solidcolorprocess.h"

class SolidColorPage : public LedPage
{
public:
  SolidColorPage(LedController* ledController) : LedPage(ledController) {}

  String url() const override
  {
    return "/solidcolor";
  }

  String title() const override
  {
    return "Solid Color";
  }

  std::vector<String> requiredArguments() const override
  {
    return {"color"};
  }

protected:
  IProcess* createProcess(const Arguments& arguments) const override
  {
    return new SolidColorProcess(strip(), hexToRGB(arguments.arg("color")));
  }

  String getArgumentLimitErrors(const Arguments& arguments) const override
  {
    String errorMessage;
    auto colorArg = arguments.arg("color");
    if (colorArg.length() != 6)
      errorMessage += "Invalid color argument. Must be 6 digit HEX\n";
    return errorMessage;
  }

};

#endif
