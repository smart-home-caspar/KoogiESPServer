#ifndef RAINBOWPAGE_H
#define RAINBOWPAGE_H

#include "ledpage.h"
#include "rainbowprocess.h"

class RainbowPage : public LedPage
{
public:
  RainbowPage(LedController* ledController) : LedPage(ledController) {}

  String url() const override
  {
    return "/rainbow";
  }

  String title() const override
  {
    return "Rainbow";
  }

  std::vector<String> requiredArguments() const override
  {
    return {"wait"};
  }

protected:
  String getArgumentLimitErrors(const Arguments& arguments) const override
  {
    return "";
  }

  IProcess* createProcess(const Arguments& arguments) const override
  {
    return new RainbowProcess(strip(), arguments.arg("wait").toInt());
  }

};

#endif
