#ifndef COLORWIPEPAGE_H
#define COLORWIPEPAGE_H

#include "ledpage.h"
#include "colorwipeprocess.h"

class ColorWipePage : public LedPage
{
public:
  ColorWipePage(LedController* ledController) : LedPage(ledController) {}

  String url() const override
  {
    return "/colorwipe";
  }

  String title() const override
  {
    return "Color Wipe";
  }

  std::vector<String> requiredArguments() const override
  {
    return {"color", "wait"};
  }

protected:
  IProcess* createProcess(const Arguments& arguments) const override
  {
    return new ColorWipeProcess(strip(), hexToRGB(arguments.arg("color")), arguments.arg("wait").toInt());
  }

  String getArgumentLimitErrors(const Arguments& arguments) const override
  {
    String errorMessage;
    auto colorArg = arguments.arg("color");
    if (colorArg.length() != 6)
      errorMessage += "Invalid color argument. Must be 6 digit HEX\n";
    return errorMessage;
  }

};

#endif
