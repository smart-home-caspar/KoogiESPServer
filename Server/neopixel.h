#ifndef NEOPIXEL_H
#define NEOPIXEL_H

#include <NeoPixelBus.h>

class NeoPixel
{
private:
  int m_pixelCount;
  NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> m_strip;
public:
  NeoPixel(int pixels)
    : m_pixelCount(pixels)
    , m_strip(pixels, 13)
  {
  }

  void setUp() {
    m_strip.Begin();
    show();
  }

  int numPixels() {
    return m_pixelCount;
  }

  void setPixelColor(int pixel, RGB rgb) {
    m_strip.SetPixelColor(pixel, RgbColor(rgb.red, rgb.green, rgb.blue));
  }

  void setAll(RGB rgb) {
    m_strip.ClearTo(RgbColor(rgb.red, rgb.green, rgb.blue));
  }

  void show() {
    m_strip.Show();
  }
};

#endif
